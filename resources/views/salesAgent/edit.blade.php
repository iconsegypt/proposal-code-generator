@extends('layouts.master')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Edit Sales Agent</h1>
</div>

<form method="post" action="{{route('salesagent.update', ['id'=>$salesagent->id])}}">
  @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">User Name</label>
    <input type="text" class="form-control"  value="{{$user->name}}" name="username" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="Email" class="form-control"  value="{{$user->email}}" name="email" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Password</label>
    <input type="password" class="form-control"  placeholder="Password" name="password" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Sales Agent Name</label>
    <input type="text" class="form-control"  value="{{$salesagent->name}}" name="name" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Code</label>
    <input type="text" class="form-control" value="{{$salesagent->code}}" name="code" required>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>
</form>


<button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete</button>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete {{$salesagent->name}} ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <form method="post" action="{{route('salesagent.delete',['id'=>$salesagent->id])}}">
          @method('delete')
          @csrf
            <button type="submit" class="btn btn-danger">Yes</button>
        </form>
      </div>
    </div>
  </div>
</div>


@if($errors)
<div class="notification is_danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>
      {{$error}}
    </li>
    @endforeach
  </ul>
</div>
@endif

@endsection