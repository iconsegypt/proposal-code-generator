@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Administrators</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <a class="btn btn-outline-secondary" href="{{route('administrator.create')}}">Create New admin</a>
    </div>
  </div>
</div>

<div class="table-responsive">
  <table class="table table-striped table-sm datatable">
    <thead>
      <tr>
        <th class="text-center">#</th>
        <th class="text-center">Name</th>
        <th class="text-center">Email</th>
        <th class="text-center">Is Suspended</th>
        <th class="text-center">Created At</th>
        <th class="text-center">Actions</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready( function () {
    var i=0;
    $('.datatable').DataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{route('administrator.data')}}",
                "type": "POST",
                data: {'_token': '{{csrf_token()}}'},
            },
            "columns": [
                {"data": null,"searchable": false,"orderable": false,name: "id",className: "text-center", render:function(data){
                    i=i+1;
                    return i;
                }},
                {"data": "name","searchable": false,"orderable": false, name:"name",className: "text-center" },
                {"data": "email","searchable": false,"orderable": false, name: "email",className: "text-center"},
                {"data": null,"searchable": false,"orderable": false, name:"suspend",className: "text-center" ,render:function(data){
                  if(data.is_suspended == 0){
                    return 'Not Suspended'
                  }
                  else{
                    return 'Suspended'
                  }
                }},
                {"data": "created_at","searchable": false,"orderable": false, name:"created_at",className: "text-center"},
                {"data": null,"searchable": false,"orderable": false, className: "text-center",render:function(data){
                  var back = '';
                  back += '<td class="text-center"><a href="{{url('administrator/edit').'/'}}'+data.id+'" class="btn btn-primary margin1px">Edit</a>';

                  if(data.is_suspended == 0){
                    back += '<a href="{{url('administrator/suspend').'/'}}'+data.id+'" class="btn btn-info margin1px">Suspend</a>'
                  }
                  else{
                    back += '<a href="{{url('administrator/suspend').'/'}}'+data.id+'" class="btn btn-info margin1px">Unsuspend</a>'
                  }

                  back +='<a href="{{url('administrator/edit').'/'}}'+data.id+'" class="btn btn-danger margin1px">Delete</a>'

                  return back;
                }}
            ],
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
@endsection