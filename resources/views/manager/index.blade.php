@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Managers</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <a class="btn btn-outline-secondary" href="{{route('manager.create')}}">Create New Manager</a>
    </div>
  </div>
</div>

<div class="table-responsive">
  <table class="table table-striped table-sm datatable">
    <thead>
      <tr>
        <th class="text-center">#</th>
        <th class="text-center">Name</th>
        <th class="text-center">Code</th>
        <th class="text-center">Created At</th>
        <th class="text-center">Actions</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready( function () {
    var i=0;
    $('.datatable').DataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{route('manager.data')}}",
                "type": "POST",
                data: {'_token': '{{csrf_token()}}'},
            },
            "columns": [
                {"data": null,"searchable": false,"orderable": false,name: "id",className: "text-center", render:function(data){
                    i=i+1;
                    return i;
                }},
                {"data": "name","searchable": true,"orderable": false, name: "name",className: "text-center"},
                {"data": "code","searchable": false,"orderable": false, name:"code",className: "text-center" },
                {"data": "created_at","searchable": false,"orderable": true, name:"created_at",className: "text-center"},
                {"data": null,"searchable": false,"orderable": false, className: "text-center",render:function(data){
                  return '<td class="text-center"><a href="{{url('manager/edit').'/'}}'+data.id+'" class="btn btn-primary margin1px">Edit</a><a href="{{url('manager/edit').'/'}}'+data.id+'" class="btn btn-danger margin1px">Delete</a>';
                }} 
            ],
            "order": [
                [3, "desc"]
            ]
        });
    });
</script>
@endsection