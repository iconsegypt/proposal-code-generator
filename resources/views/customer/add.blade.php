@extends('layouts.master')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Add Customer</h1>
</div>

<form method="post" action="{{route('customer.store')}}">
  @csrf
  @if(Auth::user()->is_admin == 1)
    <div class="form-group">
      <label>Sales Agent</label>
      <select class="form-control" id="salesagent" name="salesagent" required>
        @foreach($salesagents as $salesagent)
        <option value="{{$salesagent->id}}">{{$salesagent->name}}</option>
        @endforeach
      </select>
    </div>
  @endif
  <div class="form-group">
    <label>Customer Name</label>
    <input type="text" class="form-control" value="{{old('name')}}" placeholder="Customer Name" name="name" required>
  </div>
  <div class="form-group">
    <label>CRM ID</label>
    <input type="text" class="form-control" value="{{old('crm_id')}}" placeholder="CRM ID" name="crm_id" required>
  </div>
  <div class="form-group">
    <label>Mobile Phone</label>
    <input type="text" class="form-control" value="{{old('mobile')}}" placeholder="Mobile Phone" name="mobile" required>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary">Add</button>
  </div>

  
  @if($errors)
  <div class="notification is-danger">
    <ul>
      @foreach($errors->all() as $error)
      <li>
        {{$error}}
      </li>
      @endforeach
    </ul>
  </div>
  @endif
</form>

@endsection