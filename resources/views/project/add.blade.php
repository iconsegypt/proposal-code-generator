@extends('layouts.master')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Add Project</h1>
</div>

<form method="post" action="{{route('project.store')}}">
  @csrf


  <div class="form-group">
    <label>Customer</label>
    <select class="form-control" name="customer">
      @foreach($customers as $customer)
      <option value="{{$customer->id}}">{{$customer->name}}</option>
      @endforeach
    </select>
  </div>


  <div class="form-group">
    <label>Department</label>
    <select class="form-control" name="department">
      @foreach($departments as $department)
      <option value="{{$department->id}}">{{$department->name}}</option>
      @endforeach
    </select>
  </div>



  <div class="form-group">
    <label>Project Name</label>
    <input type="text" class="form-control" value="{{old('name')}}" placeholder="Project Name" name="name" required>
  </div>


  <div class="form-group">
    <label>Price</label>
    <input type="text" class="form-control" value="{{old('price')}}" placeholder="Price" name="price" required>
  </div>


  <div class="form-group">
    <label>Currency</label>
    <select class="form-control" name="currency">
      <option value="EGP">EGP</option>
      <option value="USD">USD</option>
    </select>
  </div>


  <div class="form-group">
    <button type="submit" class="btn btn-primary">Add</button>
  </div>

  
  @if($errors)
  <div class="notification is-danger">
    <ul>
      @foreach($errors->all() as $error)
      <li>
        {{$error}}
      </li>
      @endforeach
    </ul>
  </div>
  @endif
</form>

@endsection