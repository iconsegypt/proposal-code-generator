@extends('layouts.master')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Add Department</h1>
</div>

<form method="post" action="{{route('department.store')}}">
  @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Department Name</label>
    <input type="text" class="form-control" value="{{old('name')}}" placeholder="Department Name" name="name" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Code</label>
    <input type="text" class="form-control" value="{{old('code')}}" placeholder="Code" name="code" required>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary">Add</button>
  </div>

  
  @if($errors)
  <div class="notification is-danger">
    <ul>
      @foreach($errors->all() as $error)
      <li>
        {{$error}}
      </li>
      @endforeach
    </ul>
  </div>
  @endif
</form>

@endsection