@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Generated Codes</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <a class="btn btn-outline-secondary" href="{{Auth::user()->is_admin == 1 ? route('code.create') : route('code.create')}}">Generate New Code</a>
    </div>
  </div>
</div>

<div class="table-responsive">
  <table class="table table-striped table-sm datatable">
    <thead>
      <tr>
        <th class="text-center">#</th>
        <th class="text-center">Generated Code</th>
        <th class="text-center">Department</th>
        <th class="text-center">Manager</th>
        <th class="text-center">Client Source</th>
        <th class="text-center">Project</th>
        <th class="text-center">Sales Agent</th>
        <th class="text-center">Created At</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready( function () {
    var i=0;
    $('.datatable').DataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{route('code.data')}}",
                "type": "POST",
                data: {'_token': '{{csrf_token()}}'},
            },
            "columns": [
                {"data": null,"searchable": false,"orderable": false,name: "id",className: "text-center", render:function(data){
                    i=i+1;
                    return i;
                }},
                {"data": "generated_code","searchable": true,"orderable": false, name:"generated_code",className: "text-center" },
                {"data": "department.name","searchable": false,"orderable": false, name: "department.name",className: "text-center"},
                {"data": "manager.name","searchable": false,"orderable": false, name:"manager.name",className: "text-center" },
                {"data": "client_source.name","searchable": false,"orderable": false, name:"client_source.name",className: "text-center" },
                {"data": "project.name","searchable": false,"orderable": false, name:"project.name",className: "text-center" },
                {"data": "sales_agent.name","searchable": false,"orderable": false, name:"sales_agent.name",className: "text-center" },
                {"data": "created_at","searchable": false, name:"created_at",className: "text-center"}
            ],
            "order": [
                [7, "desc"]
            ]
        });
    });
</script>
@endsection