@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Generate New Code</h1>
</div>

<form method="post" action="{{route('code.store')}}">

  @csrf



  <div class="form-group">
    <label>Department</label>
    <select class="form-control" id="department" name="department">
      @foreach($departments as $department)
      <option value="{{$department->id}}">{{$department->name}}</option>
      @endforeach
    </select>
  </div>



  <div class="form-group">
    <label>Approved Manager</label>
    <select class="form-control" id="manager" name="manager">
      @foreach($managers as $manager)
      <option value="{{$manager->id}}">{{$manager->name}}</option>
      @endforeach
    </select>
  </div>



  <div class="form-group">
    <label>Client Source</label>
    <select class="form-control" id="clientsource" name="clientsource">
      @foreach($clientsources as $clientsource)
      <option value="{{$clientsource->id}}">{{$clientsource->name}}</option>
      @endforeach
    </select>
  </div>


@if(Auth::user()->is_admin == 1)
  <div class="form-group">
    <label>Sales Agent</label>
    <select class="form-control" id="salesagent" name="salesagent">
      <option value="0">Select Sales Agent</option>
      @foreach($salesagents as $salesagent)
      <option value="{{$salesagent->id}}">{{$salesagent->name}}</option>
      @endforeach
    </select>
  </div>
@endif

@if(Auth::user()->is_admin == 1)
<div id="salesagent_customers" style="display: none;">
@endif


  <div class="form-group">
    <label>Customer</label>
    <select class="form-control" id="customer" name="customer">
      <option value="0">Create New Customer</option>
      @foreach($customers as $customer)
      <option value="{{$customer->id}}">{{$customer->name}}</option>
      @endforeach
    </select>
  </div>



  <div class="form-group">
    <div class="form-group shadow-boxed" id="newCustomer" style="display: block">
      <label>New Customer Name</label>
      <input type="text" class="form-control" value="{{old('customername')}}" placeholder="Customer Name" name="customername"> 

      <label>CRM ID</label>
      <input type="text" class="form-control" value="{{old('crm_id')}}" placeholder="CRM ID" name="crm_id">

      <label>Customer Mobile Phone</label>
      <input type="text" class="form-control" value="{{old('mobile')}}" placeholder="Mobile Phone" name="mobile" >   
    </div>
  </div>



  <div class="form-group" id="project" style="display: none">
    <label>Project</label>
    <select class="form-control" id="project1" name="project">
      
    </select>
  </div>



  <div class="form-group">
    <div class="form-group shadow-boxed" id="newProject" style="display: block">
      <label>New Project Name</label>
      <input type="text" class="form-control" value="{{old('projectname')}}" placeholder="Project Name" name="projectname"> 

      <label>Project Price</label>
      <input type="text" class="form-control" value="{{old('price')}}" placeholder="Project Price" name="price">

      <label>Currency</label>
      <select class="form-control" name="currency">
        <option value="EGP">EGP</option>
        <option value="USD">USD</option>
      </select>
    </div>
  </div>



  <div class="form-group">
    <button type="submit" class="btn btn-primary">Generate</button>
  </div>

@if(Auth::user()->is_admin == 1)
</div>
@endif

  @if($errors)
  <div class="notification is-danger">
    <ul>
      @foreach($errors->all() as $error)
      <li>
        {{$error}}
      </li>
      @endforeach
    </ul>
  </div>
  @endif
  
</form>


@endsection


@section('scripts')

<script>
  $(document).ready(function(){
    $('#salesagent').on('change',function(e){
      var valueSelect = this.value;
      if(valueSelect == 0){
        $('#salesagent_customers').css('display','none');
      }
      else{
        $('#salesagent_customers').css('display','block'); 
      }
    });


    //show-hide customer and project form
    $('#customer').on('change',function(e){
      var valueSelected = this.value;
      if(valueSelected != 0){
        $('#project').css('display','block');
        $('#newCustomer').css('display','none');
      }else{
        $('#project').css('display','none');
        $('#newCustomer').css('display','block');
        $('#newProject').css('display','block');
      }
    });

    //show-hide project form
    $('#project').on('change',function(e){
      var idSelected = $(this).find('option:selected').attr('id');
      if(idSelected != 0){
        $('#newProject').css('display','none');
      }else{
        $('#newProject').css('display','block');
      }
    });

    $('#salesagent').on('change',function(e){
      var salesagentValue = $(this).val();
      $.ajax({
        type:"post",
        url:"{{route('code.salesagent.getcustomer')}}",
        data:{'salesagent':salesagentValue,'_token': '{{csrf_token()}}'},
        success:function(data){
          $("#customer").html('');
          $("#customer").html(data.salesagent);
        }
      });
    });


    $('#customer').on('change',function(e){
      var customerValue = $(this).val();
      if(customerValue != 0){
        $.ajax({
          type:"post",
          url:"{{route('code.customer.getproject')}}",
          data:{'customer_id':customerValue,'_token': '{{csrf_token()}}'},
          success:function(data){
            $("#project1").html('');
            $("#project1").html(data.option);
          }
        });
      }
    });
  });
</script>

@endsection