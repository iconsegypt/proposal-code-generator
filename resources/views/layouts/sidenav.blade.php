        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="{{route('code.index')}}">
                  <span data-feather="file"></span>
                  Generate Codes
                </a>
              </li>
              @if(auth::user()->is_admin == 1)
              <li class="nav-item">
                <a class="nav-link" href="{{route('department.index')}}">
                  <span data-feather="home"></span>
                  Departments
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('manager.index')}}">
                  <span data-feather="users"></span>
                  Managers
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('salesagent.index')}}">
                  <span data-feather="users"></span>
                  Sales Agents
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('clientsource.index')}}">
                  <span data-feather="users"></span>
                  Client Sources
                </a>
              </li>
              @endif
              <li class="nav-item">
                <a class="nav-link" href="{{route('customer.index')}}">
                  <span data-feather="users"></span>
                  Customers
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('project.index')}}">
                  <span data-feather="users"></span>
                  Projects
                </a>
              </li>
            </ul>

            @if(auth::user()->is_admin == 1)
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Reports</span>
              <a class="d-flex align-items-center text-muted" href="#"></a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="{{route('report.salesagent.index')}}">
                  <span data-feather="file-text"></span>
                  Sales Agent Reports
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('report.customer.index')}}">
                  <span data-feather="file-text"></span>
                  Customer Reports
                </a>
              </li>
            </ul>


            
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Admins</span>
              <a class="d-flex align-items-center text-muted" href="#"></a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="{{route('administrator.index')}}">
                  <span data-feather="users"></span>
                  Admins
                </a>
              </li>
            </ul>
            @endif
          </div>
        </nav>