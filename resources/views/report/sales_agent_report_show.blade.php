@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">{{$salesagent->name}} Report</h1>
  <h1 class="h2">Total Projects Price: {{$totalPrice}}</h1>
</div>
<div class="table-responsive">
  <table class="table table-striped table-sm datatable">
  	<thead>
  		<th class="text-center">#</th>
  		<th class="text-center">Generated Codes</th>
  		<th class="text-center">customers</th>
  		<th class="text-center">Projects Info</th>
  		<th class="text-center">Price</th>
  		<th class="text-center">Date</th>
  		<th class="text-center">Actions</th>
  	</thead>
  	<tbody>
  		
  	</tbody>
  </table>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready( function () {
    var i=0;
    $('.datatable').DataTable({
            "serverSide": true,
            "ajax": {
                "url": "{{route('report.salesagent.data')}}",
                "type": "POST",
                data: {salesagent:"{{$salesagent->id}}",from_date:"{{$from_date}}",to_date:"{{$to_date}}",'_token': '{{csrf_token()}}'},
            },
            "columns": [
                {"data": null,"searchable": false,name: "id",className: "text-center", render:function(data){
                    i=i+1;
                    return i;
                }},
                {"data": "codes.generated_code","searchable": false,"orderable": false, name: "generated_code",className: "text-center"},
                {"data": "customer.name","searchable": false,"orderable": false, name: "customer.name",className: "text-center"},
                {"data": "name","searchable": true,"orderable": false, name: "name",className: "text-center"},
                {"data": "price","searchable": false,"orderable": false, name: "project.price",className: "text-center"},
                {"data": "created_at","searchable": false,"orderable": false, name: "project.created_at",className: "text-center"},
                {"data": null,"searchable": false,"orderable": false, name: "actions",className: "text-center",render:function(data){
                	return '<a href="{{url('/report/customer')}}/'+data.customer.id+'" class="btn btn-primary margin1px">View</a>'
                }}
            ],
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
@endsection