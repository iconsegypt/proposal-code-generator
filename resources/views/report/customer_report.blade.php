@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Customers Report</h1>
</div>
<form method="post" action="{{route('report.customer.getReport')}}">
	@csrf

  <div class="row">
    <div class="col">
    	<div class="form-group">
        <label for="exampleFormControlSelect1">Customer</label>
        <select class="form-control" name="customer">
          @foreach($customers as $customer)
          <option value="{{$customer->id}}">{{$customer->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col"></div>
  </div>

  <div class="row">
    <div class="form-group col">
      <label for="exampleInputPassword1">From Date</label>
      <input type="date" class="form-control" name="from_date">
    </div>

    <div class="form-group col">
      <label for="exampleInputPassword1">To Date</label>
      <input type="date" class="form-control" name="to_date">
    </div>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary">View Report</button>
  </div>
</form>
@endsection