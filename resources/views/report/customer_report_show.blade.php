@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">{{$customer->name}} Report</h1>
</div>
<div>
  <ul>
    <div class="float-left">
    <li><h5>Sales Agent: {{$customer->salesagent->name}}</h5></li>
    <li><h5>CRM ID: {{$customer->crm_id}}</h5></li>
    </div>
    <div class="float-right mr-5">
    <li><h5>Total Projects Price: {{$totalPrice}}</h5></li>
    <li><h5>Mobile: {{$customer->mobile}}</h5></li>
    <li><h5>Created At: {{$customer->created_at}}</h5></li>
    </div>
  </ul>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <th class="text-center">#</th>
      <th class="text-center">Project</th>
      <th class="text-center">Generated Codes</th>
      <th class="text-center">Department</th>
      <th class="text-center">Price</th>
      <th class="text-center">Created At</th>
    </thead>
    <tbody>
      @foreach($projects as $i => $project)
      <tr>
        <td class="text-center">{{++$i}}</td>
        <td class="text-center">{{$project->name}}</td>
        <td class="text-center">{{$project->codes->generated_code}}</td>
        <td class="text-center">{{$project->department->name}}</td>
        <td class="text-center">{{$project->price}}</td>
        <td class="text-center">{{$project->created_at}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection