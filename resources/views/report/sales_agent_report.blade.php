@extends('layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
  <h1 class="h2">Sales Agents Report</h1>
</div>
<form method="post" action="{{route('report.salesagent.getReport')}}">
	@csrf

  <div class="row">
    <div class="col">
    	<div class="form-group">
        <label>Sales Agent</label>
        <select class="form-control" name="salesagent">
          @foreach($salesagents as $salesagent)
          <option value="{{$salesagent->id}}">{{$salesagent->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col"></div>
  </div>

  <div class="row">
    <div class="form-group col">
      <label>From Date</label>
      <input type="date" class="form-control" name="from_date">
    </div>

    <div class="form-group col">
      <label>To Date</label>
      <input type="date" class="form-control" name="to_date">
    </div>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary">View Report</button>
  </div>
</form>
@endsection