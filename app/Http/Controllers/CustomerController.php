<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Yajra\Datatables\Datatables;
use Auth;
use App\Project;
use App\SalesAgent;
use App\User;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('customer.index');
    }

    public function data(){
        if(Auth::User()->is_admin == 0){
            $customers = Customer::where('sales_agent_id', Auth::User()->id);
        }
        else{
            $customers = Customer::all();
        }

        return Datatables::of($customers)->make(true);
    }

    public function create(){
        $salesagents = User::where('is_admin',0)->get();
    	return view('customer.add',compact('salesagents'));
    }

    public function store(Request $request){
    	$request->validate([
    		'name'=>'required|min:6',
            'crm_id'=>'required|integer|unique:customers',
    		'mobile'=>'required|digits_between:7,15'
    	]);

    	$customer = new Customer;
        $customer->sales_agent_id = Auth::User()->id;
        if(Auth::user()->is_admin == 1){
            $request->validate(['salesagent'=>'required']);
            $customer->sales_agent_id = $request->salesagent;
        }else{
            $customer->sales_agent_id = Auth::User()->id;
        }
    	$customer->name = $request->name;
        $customer->crm_id = $request->crm_id;
    	$customer->mobile = $request->mobile;

    	$customer->save();

    	return redirect()->route('customer.index');
    }

    public function edit($id){
    	$customer = Customer::find($id);
    	return view('customer.edit',compact('customer'));
    }

    public function update(Request $request, $id){
    	$customer = Customer::find($id);

    	$request->validate([
    		'name'=>'required|min:6',
            'crm_id'=>['required','integer',Rule::unique('customers')->ignore($request->crm_id,'crm_id')],
            'mobile'=>'required|digits_between:7,15'
    	]);

    	if(!empty($customer)){

            $customer->name = $request->name;
            $customer->crm_id = $request->crm_id;
            $customer->mobile = $request->mobile;

            $customer->save();

            return redirect()->route('customer.index');
        }
        return redirect()->route('customer.index');
    }

    public function destroy($id){
        $customer = Customer::find($id);
        if(!empty($customer)){
            $projects = Project::where('customer_id',$id)->first();
            if(!empty($projects)){
                $message = 'can not delete, Customer have projects';
                return redirect()->route('customer.edit',['id'=>$id])->withErrors($message);
            }
            $customer->delete();
        }
        return redirect()->route('customer.index');
    }
}
