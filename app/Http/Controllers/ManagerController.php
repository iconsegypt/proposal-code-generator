<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager;
use Yajra\Datatables\Datatables;
use App\Code;

class ManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('manager.index');
    }

    public function data(){
        $managers = Manager::all();

        return Datatables::of($managers)->make(true);
    }

    public function create(){
    	return view('manager.add');
    }

    public function store(Request $request){
    	$request->validate([
    		'name'=>'required|min:6',
    		'code'=>'required|min:2|max:3'
    	]);

    	$manager = new Manager;
    	$manager->name = $request->name;
    	$manager->code = $request->code;

    	$manager->save();

    	return redirect()->route('manager.index');
    }

    public function edit($id){
    	$manager = Manager::find($id);
    	return view('manager.edit', compact('manager'));
    }

    public function update(Request $request, $id){
    	$manager = Manager::find($id);

    	$request->validate([
    		'name'=>'required|min:6',
            'code'=>'required|min:2|max:3'
    	]);

    	if(!empty($manager)){
    		$manager->name = $request->name;
    		$manager->code = $request->code;

    		$manager->save();

    		return redirect()->route('manager.index');
    	}

    	return redirect()->route('manager.index');
    }

    public function destroy($id){
        $manager = Manager::find($id);
        if(!empty($manager)){
            $coded = Code::where('manager_id',$id)->first();
            if(!empty($coded)){
                $message = 'can not delete, Manager have codes';
                return redirect()->route('manager.edit',['id'=>$id])->withErrors($message);
            }
            $manager->delete();
        }
        return redirect()->route('manager.index');
    }
}
