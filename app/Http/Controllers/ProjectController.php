<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Customer;
use App\Department;
use Yajra\Datatables\Datatables;
use Auth;
use App\Code;
use App\User;
use App\SalesAgent;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('project.index');
    }

    public function data(){
        if(Auth::User()->is_admin == 0){
            $projects = Project::with('customer','codes')->where('sales_agent_id',Auth::User()->id);
        }
        else {
            $projects = Project::with('customer','codes'); 
        }

        return Datatables::of($projects)->make(true);
    }

    public function create(){
        if(Auth::User()->is_admin == 0){
    	   $customers = Customer::where('sales_agent_id',Auth::id())->get();
        }else{
            $customers = Customer::all();
        }
        $departments = Department::all();
        $salesagents = User::where('is_admin',0)->get();
    	return view('project.add', compact('customers','departments','salesagents'));
    }

    public function store(Request $request){
    	$request->validate([
    		'customer'=>'required',
            'department'=>'required',
    		'name'=>'required|min:6',
    		'price'=>'required|integer',
            'currency'=>'required'
    	]);

    	$project = new Project;

        $project->department_id = $request->department;
    	$project->customer_id = $request->customer;
        if(Auth::user()->is_admin == 1){
            //$request->validate(['salesagent'=>'required']);
            //$project->sales_agent_id = $request->salesagent;
            $project->sales_agent_id = Customer::where('id',$request->customer)->first()->sales_agent_id;
        }else{
            $project->sales_agent_id = Auth::User()->id;
        }
    	$project->name = $request->name;
    	$project->price = $request->price;
        $project->currency = $request->currency;

    	$project->save();

    	return redirect()->route('project.index');
    }

    public function edit($id){
    	$project = Project::find($id);
    	return view('project.edit', compact('project'));
    }

    public function update(Request $request, $id){
    	$project = Project::find($id);

    	$request->validate([
            'name'=>'required|min:6',
            'price'=>'required|integer',
            'currency'=>'required'
    	]);

    	if(!empty($project)){
    		$project->name = $request->name;
    		$project->price = $request->price;
            $project->currency = $request->currency;

    		$project->save();

    		return redirect()->route('project.index');
    	}
    	

    	return redirect()->route('project.index');
    }

    public function destroy($id){
        $project = Project::find($id);
        if(!empty($project)){
            $coded = Code::where('project_id',$id)->first();
            if(!empty($coded)){
                $message = 'can not delete, project have codes';
                return redirect()->route('project.edit',['id'=>$id])->withErrors($message);
            }
            $project->delete();
        }
        return redirect()->route('project.index');
    }
}
