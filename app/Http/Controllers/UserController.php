<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('administrator.index');
    }


    public function data(){
        $users = User::where('is_admin',1);

        return Datatables::of($users)->make(true);
    }

    public function create(){
    	return view('administrator.add');
    }

    public function store(Request $request){
    	$request->validate([
    		'name' => 'required|string',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|min:6|confirmed'
    	]);

    	$user = new User;

    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = Hash::make($request->password);
    	$user->is_admin = 1;

    	$user->save();

    	return redirect()->route('administrator.index');
    }

    public function edit($id){
    	$user = User::find($id);
    	return view('administrator.edit',compact('user'));
    }

    public function update(Request $request,$id){
    	$user = User::find($id);

    	$request->validate([
    		'name' => 'required|string',
    		'email' => 'required|email',
    		'password' => 'required|min:6|confirmed',
    		'old_password' => 'required'
    	]);

    	if(!empty($user) && Hash::check($request->old_password, $user->password)){
    		$user->name = $request->name;
    		$user->email = $request->email;
    		$user->password = Hash::make($request->password);

    		$user->save();

    		return redirect()->route('administrator.index');
    	}
    	return redirect()->route('administrator.index');
    }

    public function suspend($id){
    	$user = User::find($id);

    	if(!empty($user) && $user->is_suspended == 0){
    		$user->is_suspended = 1;
    		$user->save();
    	}
    	elseif (!empty($user) && $user->is_suspended == 1) {
    		$user->is_suspended = 0;
    		$user->save();
    	}

    	return redirect()->route('administrator.index');

    }

    public function destroy($id){
        $user = User::find($id);
        if(!empty($user)){
            $user->delete();
        }
        return redirect()->route('administrator.index');
    }
}
