<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientSource;
use Yajra\Datatables\Datatables;
use App\Code;

class ClientSourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('clientSource.index');
    }

    public function data(){
        $clientsources = ClientSource::all();

        return Datatables::of($clientsources)->make(true);
    }

    public function create(){
    	return view('clientSource.add');
    }

    public function store(Request $request){
    	$request->validate([
    		'name'=>'required|min:5',
    		'code'=>'required|min:2|max:3'
    	]);


    	$clientsource = new ClientSource;
    	$clientsource->name = $request->name;
    	$clientsource->code = $request->code;

    	$clientsource->save();

    	return redirect()->route('clientsource.index');
    }

    public function edit($id){
    	$clientsource = ClientSource::find($id);
    	return view('clientSource.edit' , compact('clientsource'));
    }


    public function update(Request $request,$id){
    	$clientsource = ClientSource::find($id);

    	$request->validate([
    		'name'=>'required|min:5',
            'code'=>'required|min:2|max:3'
    	]);

    	$clientsource->name = $request->name;
    	$clientsource->code = $request->code;

    	$clientsource->save();

    	return redirect()->route('clientsource.index');
    }

    public function destroy($id){
        $clientsource = ClientSource::find($id);
        if(!empty($clientsource)){
            $coded = Code::where('client_source_id',$id)->first();
            if(!empty($coded)){
                $message = 'can not delete, client source have codes';
                return redirect()->route('clientsource.edit',['id'=>$id])->withErrors($message);
            }
            $clientsource->delete();
        }
        return redirect()->route('clientsource.index');
    }
}
