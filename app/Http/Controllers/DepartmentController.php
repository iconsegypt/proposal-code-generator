<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Yajra\Datatables\Datatables;
use App\Code;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('department.index');
    }

    public function data(){
        $departments = Department::all();

        return Datatables::of($departments)->make(true);
    }

    public function create(){
    	return view('department.add');
    }

    public function store(Request $request){
    	$request->validate([
    		'name'=>'required|min:6',
    		'code'=>'required|min:2|max:3'
    	]);

    	$department = new Department;
    	$department->name = $request->name;
    	$department->code = $request->code;

    	$department->save();

    	return redirect()->route('department.index');
    }

    public function edit($id){
    	$department = Department::find($id);
    	return view('department.edit',compact('department'));
    }

    public function update(Request $request, $id){
    	$department = Department::find($id);

    	$request->validate([
    		'name'=>'required|min:6',
            'code'=>'required|min:2|max:3'
    	]);

    	if(!empty($department)){
    		$department->name = $request->name;
    		$department->code = $request->code;

    		$department->save();

    		return redirect()->route('department.index');
    	}

    	return redirect()->route('department.index');
    }


    public function destroy($id){
        $department = Department::find($id);
        if(!empty($department)){
            $coded = Code::where('department_id',$id)->first();
            if(!empty($coded)){
                $message = 'can not delete, Department have codes';
                return redirect()->route('department.edit',['id'=>$id])->withErrors($message);
            }
            $department->delete();
        }
        return redirect()->route('department.index');
    }
}
