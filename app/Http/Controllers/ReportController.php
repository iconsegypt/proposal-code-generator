<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Customer;
use App\User;
use Yajra\Datatables\Datatables;

class ReportController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function salesagent(){
    	$salesagents = User::where('is_admin',0)->get();
    	return view('report.sales_agent_report',compact('salesagents'));
    }

    public function customer(){
        $customers = Customer::all();
        return view('report.customer_report',compact('customers'));
    }

    public function getsalesagentReport(Request $request){
        $request->validate([
            'salesagent' => 'required'
        ]);
        //find sales agent exists
    	$salesagent = User::where('is_admin',0)->find($request->salesagent);
        $from_date = $request->from_date;
        $to_date = $request->to_date;

        // if sales agent exists
    	if(!empty($salesagent)){
            //get report in a spacific time
            if(!empty($from_date) && !empty($to_date)){
                $projects = Project::where('sales_agent_id',$request->salesagent)
                    ->where('created_at','>=',$from_date)
                    ->where('created_at','<=',$to_date." 23:59:59")
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }  
            }
            //get report from a spacific date until now
            elseif (!empty($from_date)) {
                $projects = Project::where('sales_agent_id',$request->salesagent)
                    ->where('created_at','>=',$from_date)
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }
            }
            //get report to a spacific date
            elseif (!empty($to_date)) {
                $projects = Project::where('sales_agent_id',$request->salesagent)
                    ->where('created_at','<=',$to_date." 23:59:59")
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                } 
            }
            //get report all data
            else{
                $projects = Project::where('sales_agent_id',$request->salesagent)->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }
            }

            //dd($projects);
            return view('report.sales_agent_report_show',compact('salesagent','projects','totalPrice','from_date','to_date'));   
    	}

        // if sales agent does not exist redirect to the form again 
        return redirect()->route('report.salesagent.index');
    }


    public function getsalesagentdata(Request $request){
        if(!empty($request->from_date) && !empty($request->to_date)){
                $projects = Project::with('codes','customer')
                    ->where('sales_agent_id',$request->salesagent)
                    ->where('created_at','>=',$request->from_date)
                    ->where('created_at','<=',$request->to_date." 23:59:59");
            }
            //get report from a spacific date until now
            elseif (!empty($request->from_date)) {
                $projects = Project::with('codes','customer')
                    ->where('sales_agent_id',$request->salesagent)
                    ->where('created_at','>=',$request->from_date);
            }
            //get report to a spacific date
            elseif (!empty($request->to_date)) {
                $projects = Project::with('codes','customer')
                    ->where('sales_agent_id',$request->salesagent)
                    ->where('created_at','<=',$request->to_date." 23:59:59");
            }
            //get report all data
            else{
                $projects = Project::with('codes','customer')->where('sales_agent_id',$request->salesagent);
            }

        return Datatables::of($projects)->make(true);
    }


    public function getcustomerReport(Request $request){
        $request->validate([
            'customer' => 'required'
        ]);

        //find customer exists
        $customer = Customer::with('salesagent')->find($request->customer);


        //if customer exists
        if(!empty($customer)){

            //get report in a spacific time
            if(isset($request->from_date) && isset($request->to_date)){
                $projects = Project::with('codes','department')
                    ->where('customer_id',$request->customer)
                    ->where('created_at','>=',$request->from_date)
                    ->where('created_at','<=',$request->to_date." 23:59:59")
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }  
            }
            //get report from a spacific date until now
            elseif (isset($request->from_date)){
                $projects = Project::with('codes','department')
                    ->where('customer_id',$request->customer)
                    ->where('created_at','>=',$request->from_date)
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }  
            }
            //get report to a spacific date
            elseif (isset($request->to_date)) {
                $projects = Project::with('codes','department')
                    ->where('customer_id',$request->customer)
                    ->where('created_at','<=',$request->to_date." 23:59:59")
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                } 
            }
            else{
                $projects = Project::with('codes','department')
                    ->where('customer_id',$request->customer)
                    ->get();


                $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }  
            }

           // dd($customer , $projects);
            return view('report.customer_report_show',compact('projects','customer','totalPrice'));
        }

        return redirect()->route('report.customer.index');
    }

    public function customersdetails($id){
        $customer = Customer::findOrfail($id);
        if($customer){
            $projects = Project::where('customer_id',$id)->get();

            $totalPrice = 0;
                foreach ($projects as $project) {
                    $totalPrice += $project->price;
                }  
        }


        return view('report.customer_report_show',compact('customer','projects','totalPrice'));
    }
}
