<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\SalesAgent;
use App\User;
use App\Code;
use App\Customer;
use App\Project;
use Yajra\Datatables\Datatables;

class SalesAgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('salesAgent.index');
    }

    public function data(){
        $salesagents = SalesAgent::with('user');

        return Datatables::of($salesagents)->make(true);
    }

    public function create(){
    	return view('salesAgent.add');
    }

    public function store(Request $request){
    	$request->validate([
    		'username'=>'required|min:6',
    		'email'=>'required|email|unique:users',
    		'password'=>'required|min:6',
    		'name'=>'required|min:6',
    		'code'=>'required|min:2|max:3'
    	]);

    	$salesagent = new SalesAgent;
        $user = new User;

    	$user->name = $request->username;
    	$user->email = $request->email;
    	$user->password = Hash::make($request->password);
        $user->save();

        $salesagent->user_id = $user->id;
    	$salesagent->name = $request->name;
    	$salesagent->code = $request->code;

    	$salesagent->save();

    	return redirect()->route('salesagent.index');
    }

    public function edit($id){
    	$salesagent = SalesAgent::where('user_id',$id)->first();
        $user = User::where('is_admin',0)->find($id);
    	return view('salesAgent.edit', compact('salesagent','user'));
    }

    public function update(Request $request, $id){
    	$salesagent = SalesAgent::find($id);
        $user = User::where('id',$salesagent->user_id)->first();

    	$request->validate([
    		'username'=>'required|min:6',
            'email'=>'required|email',
            'password'=>'required|min:6',
            'name'=>'required|min:6',
            'code'=>'required|min:2|max:3'
    	]);


    	If(!empty($salesagent)){
    		$user->name = $request->username;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            $salesagent->name = $request->name;
            $salesagent->code = $request->code;

            $salesagent->save();

            return redirect()->route('salesagent.index');
    	}

        return redirect()->route('salesagent.index');

    	
    }


    public function destroy($id){
        $salesagent = SalesAgent::find($id);
        $user = User::where('id',$salesagent->user_id)->first();
        if(!empty($salesagent)){
            $coded = Code::where('sales_agent_id',$salesagent->user_id)->first();
            $projects = Project::where('sales_agent_id',$salesagent->user_id)->first();
            $customer = Customer::where('sales_agent_id',$salesagent->user_id)->first();
            if(!empty($coded) || !empty($projects) || !empty($customer)){
                $message = 'can not delete , client source have codes ,customers and projects';
                return redirect()->route('salesagent.edit',['id'=>$id])->withErrors($message);
            }
            $salesagent->delete();
            $user->delete();
        }
        return redirect()->route('salesagent.index');
    }
}
