<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Code;
use App\User;
use App\Department;
use App\Manager;
use App\ClientSource;
use App\SalesAgent;
use App\Customer;
use App\Project;
use Yajra\Datatables\Datatables;
use Auth;

class CodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	
    	return view('code.index', compact('codes'));
    }


    public function data(){
        if(Auth::User()->is_admin == 0){
            $codes = Code::with(['department','manager','clientSource','project','salesAgent','customer'])->where('sales_agent_id',Auth::User()->id);
        }
        else{
            $codes = Code::with(['department','manager','clientSource','project','salesAgent','customer']);
        }
        

        return Datatables::of($codes)->make(true);
    }

    public function create(){
    	$departments = Department::all();
    	$managers = Manager::all();
    	$clientsources = ClientSource::all();
    	$salesagents = User::where('is_admin',0)->get();
        if(Auth::user()->is_admin == 1){
            $customers = Customer::with(['project'=>function($query){
                        $query->where('has_code', null);
                    }])->get();
        }else{
            $customers = Customer::with(['project'=>function($query){
                        $query->where('has_code', null);
                    }])->where('sales_agent_id',Auth::id())->get();
        }
        

        //dd($customers);

    	return view('code.add', compact('departments','managers','clientsources','salesagents','customers'));
    }

    public function getsalesagentcustomers(Request $request){
        if($request->ajax()){
            $customers = Customer::where('sales_agent_id',$request->salesagent)->get();
            $data = view('code.select_customers',compact('customers'))->render();
            return response()->json(['salesagent'=>$data]);
        }
    }

    public function getcustomerproject(Request $request){
        if($request->ajax()){
            $projects = Project::where('customer_id',$request->customer_id)->where('has_code',null)->get();
            $data = view('code.select_projects',compact('projects'))->render();
            return response()->json(['option'=>$data]);
        }
    }

    public function store(Request $request){

        //validate fileds for general 
    	$request->validate([
    		'department'=>'required',
    		'manager'=>'required',
    		'clientsource'=>'required',
            'customer'=>'required'
    	]);

        $customer_id = $request->customer;
        $project_id = $request->project;


        //create new customer and project and generate code
        if($request->customer == 0){

            //validate fileds for create customer and project
            $request->validate([
                'customername'=>'required|min:6',
                'mobile'=>'required|digits_between:7,15',
                'crm_id'=>'required|integer|unique:customers',
                'projectname'=>'required|min:6',
                'price'=>'required|integer',
                'currency'=>'required'
            ]);


            //define customer data
            $customer = new Customer;
            if(Auth::user()->is_admin == 1){
                $request->validate(['salesagent'=>'required']);
                $customer->sales_agent_id = $request->salesagent;
            }else{
                $customer->sales_agent_id = Auth::User()->id;
            }
            $customer->name = $request->customername;
            $customer->crm_id = $request->crm_id;
            $customer->mobile = $request->mobile;

            //save customer
            $customer->save();

            //get customer id for creating project and generating code
            $customer_id = $customer->id;

            //define project data
            $project = new Project;

            $project->department_id = $request->department;
            $project->customer_id = $customer_id;
            if(Auth::user()->is_admin == 1){
                $request->validate(['salesagent'=>'required']);
                $project->sales_agent_id = $request->salesagent;
            }else{
                $project->sales_agent_id = Auth::User()->id;
            }
            $project->name = $request->projectname;
            $project->price = $request->price;
            $project->currency = $request->currency;

            //save project
            $project->save();

            //get project id for generating code
            $project_id = $project->id;
        }
        //create new project and generate code
        elseif($request->customer != 0 && $request->project == 'newproject'){

            //validate fileds for create project
            $request->validate([
                'projectname'=>'required|min:6',
                'price'=>'required|integer',
                'currency'=>'required'
            ]);


            //define project data
            $project = new Project;

            $project->department_id = $request->department;
            $project->customer_id = $request->customer;
            if(Auth::user()->is_admin == 1){
                $request->validate(['salesagent'=>'required']);
                $project->sales_agent_id = $request->salesagent;
            }else{
                $project->sales_agent_id = Auth::User()->id;
            }
            $project->name = $request->projectname;
            $project->price = $request->price;
            $project->currency = $request->currency;

            //save project
            $project->save();

            //get project id for generating code
            $project_id = $project->id;
        }

        //generate code


        //get departments, managers, client source and sales agent data for using thier codes
        $departments = Department::find($request->department);
        $managers = Manager::find($request->manager);
        $clientsources = ClientSource::find($request->clientsource);
        if(Auth::user()->is_admin == 1){
            $salesagents = SalesAgent::where('user_id',$request->salesagent)->first();
        }else{
            $salesagents = SalesAgent::where('user_id',Auth::User()->id)->first();
        }


        //generate code number -0001- , -0002-
        $codecount = Code::count();
        $numlength = strlen((string)$codecount);

        $codecount = $codecount+1;

        if($numlength == 1){
            $codenum = '000'.$codecount;
        }
        elseif ($numlength == 2) {
            $codenum = '00'.$codecount;
        }
        elseif ($numlength == 3) {
            $codenum = '0'.$codecount;
        }
        else{
            $codenum = $codecount;
        }


        //saving codes data
        $code = new code;
        $code->department_id = $request->department;
        $code->manager_id = $request->manager;
        $code->client_source_id = $request->clientsource;
        if(Auth::user()->is_admin == 1){
            $code->sales_agent_id = $request->salesagent;
        }else{
            $code->sales_agent_id = Auth::User()->id;
        }
        $code->customer_id = $customer_id;
        $code->project_id = $project_id;

        //generate codes
        $code->generated_code = $departments->code.$managers->code.'-'.$codenum.'-'.$clientsources->code.$salesagents->code;

        //save codes
        $code->save();

        //alert project table that these project has code so 
        $project = Project::find($project_id);
        $project->has_code = 1 ;
        $project->save();

        return redirect()->route('code.index');

    }
}
