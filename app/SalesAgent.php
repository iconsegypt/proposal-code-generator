<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesAgent extends Model
{
    public function codes(){
    	return $this->hasMany('App\Code');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function projects(){
    	return $this->hasMany('App\Project');
    }

    public function customers(){
    	return $this->hasMany('App\Customer');
    }
}
