<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function customer(){
    	
    	return $this->belongsTo('App\Customer');
    }

    public function codes(){

    	return $this->hasOne('App\Code');
    }

    public function salesAgent(){

    	return $this->belongsTo('App\User');
    }

    public function department(){

        return $this->belongsTo('App\Department');
    }
}
