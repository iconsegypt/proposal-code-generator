<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function project(){
    	return $this->hasMany('App\Project');
    }

    public function code(){
    	return $this->hasMany('App\Code');
    }

    public function salesAgent(){

    	return $this->belongsTo('App\User');
    }
}
