<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    public function department(){
    	return $this->belongsTo('App\Department');
    }

    public function manager(){
    	return $this->belongsTo('App\Manager');
    }

    public function clientSource(){
    	return $this->belongsTo('App\ClientSource');
    }

    public function project(){
    	return $this->belongsTo('App\Project');
    }

    public function salesAgent(){
    	return $this->belongsTo('App\User');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
