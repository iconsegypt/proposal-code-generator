<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/','Auth\LoginController@showLoginForm');
Route::get('/home', 'CodeController@index')->name('home');

// Code Routes
Route::group(['prefix' => 'code'], function() {
	Route::get('/', 'CodeController@index')->name('code.index');
	Route::post('/', 'CodeController@data')->name('code.data');
	Route::get('/create', 'CodeController@create')->name('code.create');
	Route::post('/store', 'CodeController@store')->name('code.store');
	Route::post('customer', 'CodeController@getcustomerproject')->name('code.customer.getproject');
	Route::post('salesagent', 'CodeController@getsalesagentcustomers')->name('code.salesagent.getcustomer');
});

Route::group(array('middleware' => ['admin']), function () {
	// client Sources routes
	Route::group(['prefix' => 'clientsource'],function(){
		Route::get('/','ClientSourceController@index')->name('clientsource.index');
		Route::post('/','ClientSourceController@data')->name('clientsource.data');
		Route::get('/create','ClientSourceController@create')->name('clientsource.create');
		Route::post('/store','ClientSourceController@store')->name('clientsource.store');
		Route::get('/edit/{id}','ClientSourceController@edit')->name('clientsource.edit');
		Route::post('/edit/{id}','ClientSourceController@update')->name('clientsource.update');
		Route::delete('/delete/{id}','ClientSourceController@destroy')->name('clientsource.delete');
	});


	// Department Routes
	Route::group(['prefix' => 'department'],function(){
		Route::get('/','DepartmentController@index')->name('department.index');
		Route::post('/','DepartmentController@data')->name('department.data');
		Route::get('/create','DepartmentController@create')->name('department.create');
		Route::post('/store','DepartmentController@store')->name('department.store');
		Route::get('/edit/{id}','DepartmentController@edit')->name('department.edit');
		Route::post('/edit/{id}','DepartmentController@update')->name('department.update');
		Route::delete('/delete/{id}','DepartmentController@destroy')->name('department.delete');
	});


	//managers routes
	Route::group(['prefix' => 'manager'],function(){
		Route::get('/','ManagerController@index')->name('manager.index');
		Route::post('/','ManagerController@data')->name('manager.data');
		Route::get('/create','ManagerController@create')->name('manager.create');
		Route::post('/store','ManagerController@store')->name('manager.store');
		Route::get('/edit/{id}','ManagerController@edit')->name('manager.edit');
		Route::post('/edit/{id}','ManagerController@update')->name('manager.update');
		Route::delete('/delete/{id}','ManagerController@destroy')->name('manager.delete');
	});


	//sales Agents routes
	Route::group(['prefix' => 'salesagent'],function(){
		Route::get('/','SalesAgentController@index')->name('salesagent.index');
		Route::post('/','SalesAgentController@data')->name('salesagent.data');
		Route::get('/create','SalesAgentController@create')->name('salesagent.create');
		Route::post('/store','SalesAgentController@store')->name('salesagent.store');
		Route::get('/edit/{id}','SalesAgentController@edit')->name('salesagent.edit');
		Route::post('/edit/{id}','SalesAgentController@update')->name('salesagent.update');
		Route::delete('/delete/{id}','SalesAgentController@destroy')->name('salesagent.delete');
	});

	//administration
	Route::group(['prefix' => 'administrator'],function(){
		route::get('/', 'UserController@index')->name('administrator.index');
		route::post('/', 'UserController@data')->name('administrator.data');
		route::get('/create', 'UserController@create')->name('administrator.create');
		route::post('/store', 'UserController@store')->name('administrator.store');
		route::get('/edit/{id}', 'UserController@edit')->name('administrator.edit');
		route::post('/edit/{id}', 'UserController@update')->name('administrator.update');
		route::delete('/delete/{id}', 'UserController@destroy')->name('administrator.delete');
		route::get('/suspend/{id}', 'UserController@suspend')->name('administrator.suspend');
	});

	//reports
	Route::group(['prefix' => 'report'],function(){
		Route::get('/salesagent','ReportController@salesagent')->name('report.salesagent.index');
		Route::post('/salesagent','ReportController@getsalesagentReport')->name('report.salesagent.getReport');
		Route::post('/salesagent/data','ReportController@getsalesagentdata')->name('report.salesagent.data');
		Route::get('/customer','ReportController@customer')->name('report.customer.index');
		Route::post('/customer','ReportController@getcustomerReport')->name('report.customer.getReport');
		Route::get('/customer/{id}','ReportController@customersdetails')->name('report.customer.detail');
	});
});

//customers routes
Route::group(['prefix' => 'customer'],function(){
	Route::get('/','CustomerController@index')->name('customer.index');
	Route::post('/','CustomerController@data')->name('customer.data');
	Route::get('/create','CustomerController@create')->name('customer.create');
	Route::post('/store','CustomerController@store')->name('customer.store');
	Route::get('/edit/{id}','CustomerController@edit')->name('customer.edit');
	Route::post('/edit/{id}','CustomerController@update')->name('customer.update');
	Route::delete('/delete/{id}','CustomerController@destroy')->name('customer.delete');
});


//projects routes
Route::group(['prefix' => 'project'],function(){
	Route::get('/','ProjectController@index')->name('project.index');
	Route::post('/','ProjectController@data')->name('project.data');
	Route::get('/create','ProjectController@create')->name('project.create');
	Route::post('/store','ProjectController@store')->name('project.store');
	Route::get('/edit/{id}','ProjectController@edit')->name('project.edit');
	Route::post('/edit/{id}','ProjectController@update')->name('project.update');
	Route::delete('/delete/{id}','ProjectController@destroy')->name('project.delete');
});
